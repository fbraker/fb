﻿using UnityEngine;

public class AttackTwoState : IState
{

    public float m_duration { get; set; }
    // Max duration of action for JumpState
    public float actionTime
    {
        get
        {
            return .35F;
        }
    }

    StateMachine stateMachine;

    public AttackTwoState(StateMachine stateMachine)
    {
        this.stateMachine = stateMachine;
    }

    public void stopPlayer()
    {
        //m_duration += Time.deltaTime;
        stateMachine.CRigidbody.velocity = Vector2.zero;
        stateMachine.IsMoving = false;
        //if (m_duration > actionTime)
        if (!stateMachine.IsAttackTwo)
        {
            //stateMachine.IsAttackTwo = false;
            stateMachine.SetState(stateMachine.GetIdleState(), this);
        }
    }

    public void movePlayer(float moveSpeed)
    {
       // m_duration += Time.deltaTime;
        stateMachine.CRigidbody.velocity = Vector2.zero;
        //if (m_duration > actionTime)
        if (!stateMachine.IsAttackTwo)
        {
            stateMachine.IsMoving = true;
            stateMachine.SetState(stateMachine.GetMoveState(), this);
        }
    }

    public void jumpPlayer(float jumpForce)
    {
        throw new System.NotImplementedException();
    }


    public void jerkPlayer(float force)
    {
        throw new System.NotImplementedException();
    }

    public void blockPlayer()
    {
        //m_duration += Time.deltaTime;
        //if (m_duration > actionTime)
        if (!stateMachine.IsAttackTwo)
        {
            //stateMachine.IsAttackTwo = false;
            stateMachine.SetState(stateMachine.GetBlockState(), this);
        }
    }

    public void attackOne()
    {
        stateMachine.IsAttackOne = true;
        stateMachine.SetState(stateMachine.GetAttackOneState(), this);
    }


    public void specialAbility()
    {
        throw new System.NotImplementedException();
    }


    public void attackTwo()
    {
        throw new System.NotImplementedException();
    }
}
